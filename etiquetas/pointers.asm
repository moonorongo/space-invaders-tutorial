// used by fn_copyToScreenColor
.const fn_lsbCopyAddress = $0e
.const fn_msbCopyAddress = $0f 


// PLAYER 1 POINTERS
.const P1_SCORE   = $c000
.const P1_STATUS = $c001         // P1_STATUS se utiliza en la maquina de estados de JP1
.const P1_FIRE_STATUS     = $02


// GENERIC
.const SKIP_RETRACE_COUNTER = $03
