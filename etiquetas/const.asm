// VIC II
.const RASTER_POSITION = $d012
.const SPR_ACTIVE = $d015
.const BORDER_COLOR = $d020
.const PAPER_COLOR = $d021
.const INTERRUPT_STATUS = $d019
.const SPR_EXPAND_X = $d01d    
.const SPR_XBIT8  = $d010


// PLAYER 1
.const P1 = 1
.const P1_SPR = $21
.const P1_EXPLODE_SPR = $24 // comienzo del vector de animacion de explosion
.const P1_COLOR  = 5 // verde


// PLAYER 1 VIC CONST
.const P1_SPR_COLOR = $d027
.const P1_SPR_POINTER  = $07f8
.const P1_X_POSITION = $d000
.const P1_Y_POSITION = $d001
.const P1_JOY = $dc00

.const FLOOR_POSITION = 219
.const TOP_POSITION = 52
.const SKIP_RETRACE_MAX = 4

// SCREEN
.const SCREEN    = $0400
.const SCREEN_COLOR = $d800

// JOYSTICKS
.const JOYSTICK1 = $dc01

// JOYSTICK MOVEMENTS
.const JOY_FIRE = 16



// GENERAL
.const RANDOM_NUMBER = $10
.const INTERNAL_COUNTER = $09  // contador interno que voy a ir incrementando, de uso general.

// TICKS
.const TICK4 = $0A       
.const TICK64    = $0B   

