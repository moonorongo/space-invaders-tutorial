// fn_copyToScreen: copia desde la direccion que especificamos en las direciones 
//             fn_lsbCopyAddress & fn_msbCopyAddress, hasta la direccion de pantalla $0400 
// ejemplo:
//                ldx            #<screen
//                stx            fn_lsbCopyAddress
//                ldx            #>screen
//                stx            fn_msbCopyAddress
//                jsr            copyToScreen


fn_copyToScreen:
{
// posiciones de pantalla 0 - 255 
          ldy            #$00
loop:
          lda            (fn_lsbCopyAddress),y
          sta            SCREEN,y  
          
          iny
          cpy            #$00       
          bne            loop     
          
// posiciones de pantalla 256 - 512
          ldy            #$00
          inc            fn_msbCopyAddress
loop2:
          lda            (fn_lsbCopyAddress),y
          sta            SCREEN + $100,y
          
          iny
          cpy            #$00       
          bne            loop2

// posiciones de pantalla 513 - 768
          ldy            #$00
          inc            fn_msbCopyAddress
loop3:
          lda            (fn_lsbCopyAddress),y
          sta            SCREEN + $200,y
          
          iny
          cpy            #$00       
          bne            loop3

// 768 - 1000
          ldy            #$00
          inc            fn_msbCopyAddress
loop4:
          lda            (fn_lsbCopyAddress),y
          sta            SCREEN + $300,y
          
          iny
          cpy            #232     
          bne            loop4
          
          rts
}              

// fn_copyToScreenColor: copia desde la direccion que especificamos en las direciones 
//             fn_lsbCopyAddress & fn_msbCopyAddress, hasta la direccion de color de pantalla
fn_copyToScreenColor:
{
// 0- 255 
          ldy            #$00
loop:
          lda            (fn_lsbCopyAddress),y
          sta            SCREEN_COLOR,y  
          
          iny
          cpy            #$00       
          bne            loop     
          
// 256 - 512
          ldy            #$00
          inc            fn_msbCopyAddress
loop2:
          lda            (fn_lsbCopyAddress),y
          sta            SCREEN_COLOR + $100,y
          
          iny
          cpy            #$00       
          bne            loop2

// 513 - 768
          ldy            #$00
          inc            fn_msbCopyAddress
loop3:
          lda            (fn_lsbCopyAddress),y
          sta            SCREEN_COLOR + $200,y
          
          iny
          cpy            #$00       
          bne            loop3

// 768 - 1000
          ldy            #$00
          inc            fn_msbCopyAddress
loop4:
          lda            (fn_lsbCopyAddress),y
          sta            SCREEN_COLOR + $300,y
          
          iny
          cpy            #232     
          bne            loop4
          
          rts
}              
