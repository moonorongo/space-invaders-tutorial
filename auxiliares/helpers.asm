// recibe un numero en A, y retorna unidad, decena y centena en A,X,Y 
convert2number:
{
          ldy            #$2f      
          ldx            #$3a      
          sec
l100:     iny
          sbc            #100      
          bcs            l100     
          
l10:      dex
          adc            #10       
          bmi            l10      
          
          adc            #$2f      
          rts
}                        


// guarda en la etiqueta 'RANDOM_NUMBER' un numero aleatorio 0-255
randomGenerator:
{
          lda            RANDOM_NUMBER
          beq            doEor
          asl
          beq            noEor
          bcc            noEor
doEor:    eor            #$1d
noEor:    sta            RANDOM_NUMBER
          rts
}          

 
