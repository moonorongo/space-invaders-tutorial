.macro unsetB8(spriteNumber) {
          lda            SPR_XBIT8 
          and            #255 - spriteNumber
          sta            SPR_XBIT8
}

.macro setB8(spriteNumber) {
          lda            SPR_XBIT8 
          ora            #spriteNumber
          sta            SPR_XBIT8
}