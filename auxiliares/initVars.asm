initVars:
          cld                      // clear decimal flag.
          
          lda            #$00      // borde y fondo negro
          sta            BORDER_COLOR     
          sta            PAPER_COLOR


          //lda            #skiprtCant      // inicializo skiprt (conteo hasta hacer un wait retrace)
          //sta            skiprt
          
          ldx            #0        // contador hits jetpac 1 y 2
          stx            P1_SCORE
          
          lda            #$05      
          sta            SPR_ACTIVE //activamos el sprite 0 y 2
          
          lda            #$0       // fire flag 
          sta            P1_FIRE_STATUS     
          sta            P1_STATUS     
          
          
          ldx            #0        
          stx            INTERNAL_COUNTER
          
          ldx            #4
          stx            TICK4     
          
          ldx            #64
          stx            TICK64


          ldx            TIMER_LO    // inicializamos el generador de numeros aleatorios
          stx            RANDOM_NUMBER
          
{         
          // reseteamos los registros del SID 
/*          ldy #0
          lda #0
          sty sound_fire1_freq  // seteo en 0 la freq de disparo
          sty sound_fire2_freq  
loop:
          sta sidPtr,y
          iny 
          cpy #25
          bne loop

          // configuramos canal 1
          ldx #16+15            // volumen maximo 
          stx sid_vol           // filtro seteado como pasa bajo
          ldx #0
          stx sid_ad1
          ldx #100
          stx sid_hfreq1
          ldx #15*16+5          // volumen de sostenimiento max
          stx sid_sr1           // poca relajacion
          
          // configuramos canal 2
          ldx #0
          stx sid_ad2
          ldx #100
          stx sid_hfreq2
          ldx #15*16+0          // volumen de sostenimiento MENOS
          stx sid_sr2           
          
          ldx #0*16+2          // filtro voz 2, sin resonancia
          stx 54295
          
          ldx #40
          stx 54294            // frecuencia resonancia en 40
          
          // configuramos canal 3
          ldx #0
          stx sid_ad3
          ldx #3
          stx sid_hfreq3
          ldx #15*16+9          // volumen de sostenimiento max
          stx sid_sr3           // tiempo de relajacion medio          
*/          
}
          
          rts
